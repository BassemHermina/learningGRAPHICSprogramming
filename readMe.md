## Trying to learn graphics programming in depth, using webGL.

Online demo available at [https://bassemhermina.gitlab.io/learningGRAPHICSprogramming].

> Remember that it's not of benefit to me to hide the OpenGL API calls and stuff and create many abstraction layers as they are more error prone! and in fact that's what I am learning, to get used to the API and become better in creating more complex stuff using it.

- for offline running and testing use nodejs's http-server to serve the file, and open index.html

----
## What I want to learn?
1. to be able to quickly use the graphics API professionally.
2. get used to implementing complex papers that are not easy to grasp.
3. to know and understand more in depth graphics concepts/ideas/topics.

----


pdfs used:
real time rendering.
illuminationModels4up.
GPU Gems.


**TODO**

* shaders and program compiler file.  **done** @ _startup-project_
* camera and camera controls file. **done** @ _startup-project_
* host on gitlab pages. **done**
* objloader. **done** @ _startup-project_
* image loaders and texture mapping. **done** @ _startup-project_
* directional light (Blinn-Phong illumination model) **done** @ _directional-light_
* automate model loading / rendering for multiple model loadings **done** @ _direct-light-model-loading-918_
* make model loading async **done** @ _direct-light-model-loading-918_
* cubemaps -> into direct-light-model-loading-918
* shadow mapping -> into shadow-mapping (with all code in 918) may be called 918.1 as it adds functionality to the base project
* ! start GPU gems, and add desired chapter into this todo
* environment light
* normal mapping into lighting model
* parallax mapping into lighting model
* Cook Torrance physical based render:  BRDF equation: Fresnel, micro facets and distribution "article - physically based rendering - Cook-Torrance - codinglabs.com"
* maps to all properties (roughness, metalicness, shininess, albedo, specular?, ...)
* render to texture, post processing
* deferred shading?
* GPU compute shader path tracing? , hwa webGL fih compute shader? https://www.shadertoy.com/view/lsyyWG


----
## changelog
* 29-Mar-2018 startup-project
* 13-Apr-2018 created main index page
* 13-Apr-2018 created direct-light page
* 19-Apr-2018 created direct-light-model-loading-918 page

> Deploying on GitLab pages steps
https://www.youtube.com/watch?v=TWqh9MtT4Bg
