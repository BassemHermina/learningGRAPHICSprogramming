export var initializeGL =  function (){
  window.canvas = document.getElementById("canvas");
  window.gl = canvas.getContext("webgl");
  window.ctx = WebGLDebugUtils.makeDebugContext(gl);

  if (!gl) {
    console.log("Error initializig webgl!");
    return;
  }

  // set screen clear color
  gl.clearColor(0, 128, 255, 1.0);
  gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
  gl.enable(gl.DEPTH_TEST);
	gl.enable(gl.CULL_FACE);
	gl.frontFace(gl.CCW);
  gl.cullFace(gl.BACK);
}
