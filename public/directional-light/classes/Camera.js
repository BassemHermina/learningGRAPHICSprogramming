
/*
  class Camera
  - constructor: creates the camera object and assigns the event handlers
                 that are related to the camera controllers
  - computeMatricesFromInputs: called by the event handlers to refresh/compute
                               a new view matrix
  - getViewMatrix: returns the new viewMatrix
*/

export class Camera{

  constructor(prog){

    /*
     parameters needed to calculate the camera position and target
    */
    this.position =[0,0,5];
    this.horizontalAngle = 3.14;
    this.verticalAngle = 0;
    this.initialFoV = 45;

    //vectors
    this.direction = [];
    this.right = [];
    this.up = [];

    this.mouseSpeed = 0.0001;
    this.movementSpeed = 0.008;
    window.movementX = 0;
    window.movementY = 0;

    this.clickSaver = {
      key: null,
      pressed: false
    }

    /*
     initializing the view/projections matrices to a default position and target
     so as not to wait for mouse/key events. and to make them ready whenever
     needed in the main function flow
    */
    this.viewMatrix = new Float32Array(16);
    this.projMatrix = new Float32Array(16);
    mat4.lookAt(this.viewMatrix, this.position, [0,0,0], [0,1,0]); // (target, position of camera, center to look at, up)
    mat4.perspective(this.projMatrix, glMatrix.toRadian(this.initialFoV), 800/600, 0.1,1000); //( , field of view, aspect ratio, near, far)


    /*
     add camera control keys event handlers to the document body
     for any key pressed. update the matrices by the input and set the new
     view matrix in the shader uniform location (set inside the main function)
    */
    document.body.addEventListener("keydown", function (e){
      e.ttype = null;
      if (e.key == "w" || e.key == "a" || e.key == "s" || e.key == "d" || e.key == "q" || e.key == "e" || camera.clickSaver.pressed){
        camera.clickSaver.pressed = true;
        camera.clickSaver.key = e.key;
        e.customKey = e.key; //as i can't set the value key explicitly
      }
    }, false);

    document.body.addEventListener("keyup", function (e){
      camera.clickSaver.pressed = false;
    }, false);

    /*
     add camera control mouse event handlers to the document body
     - first it sets the mouse movement from the event as locking
       the mouse disable the mouse position flag in the window
     - update the matrices by the input and set the new
     view matrix in the shader uniform location (set inside the main function
    */
    window.canvas.addEventListener("mousemove", function(e){
      window.movementX = e.movementX ||
          e.mozMovementX          ||
          e.webkitMovementX       ||
          0,
      window.movementY = e.movementY ||
          e.mozMovementY      ||
          e.webkitMovementY   ||
          0;
      e.ttype = "mouse";
      camera.computeMatricesFromInputs(e);
      var viewMatrix = camera.getViewMatrix();
      // update the view matrix in the shader - the Uniform position is set in the main function
      gl.uniformMatrix4fv(camera.matViewUniformLocation, gl.FALSE, viewMatrix);
    }, false);


    /*
     add mouse click event handler to lock the mouse
     https://www.html5rocks.com/en/tutorials/pointerlock/intro/
    */
    window.canvas.addEventListener("click", function (e){
      //lock and hide the mouse
      window.canvas.requestPointerLock = window.canvas.requestPointerLock ||
             window.canvas.mozRequestPointerLock ||
             window.canvas.webkitRequestPointerLock;
      // Ask the browser to lock the pointer
      window.canvas.requestPointerLock();
    });


    /*
     handling touch events
     same as mouse events but difference is that mouse events gets
     the delta movement but here the touch only gets the position of the touch
     - I use the first touching finger, I can use many but I use e.touches[0]
     - I calculate the movementX from the position of the start of touch and the current position
       then call the same function that handles the mouse movement , as I already set what it needs
    */
    window.canvas.addEventListener('touchmove', function(e) {
      e.preventDefault();
      e.ttype = "mouse";
      // now i need to get the movementX and Y from the e.touches[0].getX and getY
      // using the last touch position. which is saved in touch start and deleted in touch end?
      // required for the computeMatricesFromInputs()
      window.movementX = e.touches[0].pageX - window.LastTouchX;
      window.movementY = e.touches[0].pageY - window.LastTouchY;

      //then normal behaviour
      camera.computeMatricesFromInputs(e);
      var viewMatrix = camera.getViewMatrix();
      gl.uniformMatrix4fv(camera.matViewUniformLocation, gl.FALSE, viewMatrix);

      //then save current touch
      window.LastTouchX = e.touches[0].pageX;
      window.LastTouchY = e.touches[0].pageY;
    });

    window.canvas.addEventListener('touchstart', function(e) {
      e.preventDefault();
      window.LastTouchX = e.touches[0].pageX;
      window.LastTouchY = e.touches[0].pageY;
    });
  }

  /*
   recalculate the view-matrix-parameters according to the type of input
   - for mouse input, recalculate the direction (front), the right, and the up vectors
   - for keys, update the position and target of the Camera
   http://www.opengl-tutorial.org/beginners-tutorials/tutorial-6-keyboard-and-mouse/
  */
  computeMatricesFromInputs(e){
    //fi moshkla lma bnd5ol l function di mn event keyboard fa kol l 7agat eli byst5dmha 7ettet el mouse di btb2a ghalat
    //fa btbawwaz direction l 7raka
    if (e.ttype == "mouse"){
      // Get mouse position -- not used here now!
      window.xPos = (window.Event) ? e.pageX : event.clientX + (document.documentElement.scrollLeft ? document.documentElement.scrollLeft : document.body.scrollLeft);
      window.yPos = (window.Event) ? e.pageY : event.clientY + (document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop);

      // Compute new orientation
      this.horizontalAngle += this.mouseSpeed * window.deltaTime  * (-window.movementX );
      this.verticalAngle   += this.mouseSpeed * window.deltaTime  * (-window.movementY );

      this.direction = [Math.cos(this.verticalAngle) * Math.sin(this.horizontalAngle),Math.sin(this.verticalAngle), Math.cos(this.verticalAngle) * Math.cos(this.horizontalAngle)];
      this.right = [Math.sin(this.horizontalAngle - 3.14/2),0,Math.cos(this.horizontalAngle - 3.14/2)];
      this.up = [0,0,0]; vec3.cross(this.up, this.right,this.direction);
    }
    // Move forward
    if (e.customKey == "w"){
      let temp = [0,0,0];
      vec3.scale(temp, this.direction,window.deltaTime * this.movementSpeed); //mutiply a vector by a number
      vec3.add(this.position, this.position, temp);
    }
    // Move backward
    if (e.customKey == "s"){
      let temp = [0,0,0];
      vec3.scale(temp, this.direction,window.deltaTime * this.movementSpeed); //mutiply a vector by a number
      vec3.sub(this.position, this.position, temp);
    }
    // Strafe right
    if (e.customKey == "d"){
      let temp = [0,0,0];
      vec3.scale(temp, this.right,window.deltaTime * this.movementSpeed); //mutiply a vector by a number
      vec3.add(this.position, this.position, temp);
    }
    // Strafe left
    if (e.customKey == "a"){
      let temp = [0,0,0];
      vec3.scale(temp, this.right,window.deltaTime * this.movementSpeed); //mutiply a vector by a number
      vec3.sub(this.position, this.position, temp);
    }
    if (e.customKey == "e"){
      let temp = [0,0,0];
      vec3.scale(temp, this.up,window.deltaTime * this.movementSpeed); //mutiply a vector by a number
      vec3.add(this.position, this.position, temp);
    }
    if (e.customKey == "q"){
      let temp = [0,0,0];
      vec3.scale(temp, this.up,window.deltaTime * this.movementSpeed); //mutiply a vector by a number
      vec3.sub(this.position, this.position, temp);
    }
  }

  /*
   calculates the view matrix and returns it to whoever needs it
   position is : position
   target is   : position + direction (front)
   up is       : up
  */
  getViewMatrix(){
    let temp = new Float32Array(16);
    let target = [0,0,0];
    vec3.add(target, this.position, this.direction);
    // Camera matrix
    mat4.lookAt(temp, this.position, target, this.up); // (target, position of camera, center to look at, up)
    return temp;
  }

}
