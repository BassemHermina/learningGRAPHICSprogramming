class objLoader{

  constructor(){

  }

  loadMeshData(string) {
    var lines = string.split("\n");
    var positions = [];
    var normals = [];
    var texturePos = []; //ADDED by me
    var vertices = [];


    for ( var i = 0 ; i < lines.length ; i++ ) {
      var parts = lines[i].trimRight().split(' ');
      if ( parts.length > 0 ) {
        switch(parts[0]) {
          case 'v':  //vertex positions
          positions.push(
            vec3.fromValues(
              parseFloat(parts[1]),
              parseFloat(parts[2]),
              parseFloat(parts[3])
            ));
            break;
          case 'vn': //vertex normals
            normals.push(
              vec3.fromValues(
                parseFloat(parts[1]),
                parseFloat(parts[2]),
                parseFloat(parts[3])
            ));
            break;
          case 'vt': //vertex texture
            texturePos.push(
              vec2.fromValues(
                parseFloat(parts[1]),
                parseFloat(parts[2])
            ));
            break;
          case 'f': {
            var f1 = parts[1].split('/');
            var f2 = parts[2].split('/');
            var f3 = parts[3].split('/');
            //this obj loader rearranges vertices instead
            //of using the indices.

            //for every point in the facet (indices)
            //we push it's position, normal, uv
            Array.prototype.push.apply(
              vertices, positions[parseInt(f1[0]) - 1]
            );
            Array.prototype.push.apply(
              vertices, normals[parseInt(f1[2]) - 1]
            );
            Array.prototype.push.apply(
              vertices, texturePos[parseInt(f1[1]) - 1]
            );

            Array.prototype.push.apply(
              vertices, positions[parseInt(f2[0]) - 1]
            );
            Array.prototype.push.apply(
              vertices, normals[parseInt(f2[2]) - 1]
            );
            Array.prototype.push.apply(
              vertices, texturePos[parseInt(f2[1]) - 1]
            );

            Array.prototype.push.apply(
              vertices, positions[parseInt(f3[0]) - 1]
            );
            Array.prototype.push.apply(
              vertices, normals[parseInt(f3[2]) - 1]
            );
            Array.prototype.push.apply(
              vertices, texturePos[parseInt(f3[1]) - 1]
            );
            break;
          }
        }
      }
    }

    //take care, this objLoader returns and array of the vertices and normals together,
    //to use them, use the vertexAttributePointers to use parts of the array for vertices or normals
    //divide by 6 as every point have xyz position and xyz for normal
    var vertexCount = vertices.length / 8;
    console.log("Loaded mesh with " + vertexCount + " vertices");
    return {
      primitiveType: 'TRIANGLES',
      vertices: new Float32Array(vertices),
      vertexCount: vertexCount
    };
  }
}

export const obj = new objLoader();
