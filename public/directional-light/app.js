"use strict";

// NOTE
// this javascript logic file is designed to run in a synchronus manner
// so it is easily understood, you can first check the imported files (as they are run when imported)
// then go with the init() function untill the end

import {Program} from './classes/Program.js'
import {Shader} from './classes/Shader.js';
import {initializeGL} from './init.js';
import {Camera} from './classes/Camera.js';
import {obj} from './classes/objLoader.js';

import {fragmentShaderText as testShaderFStext} from './Shaders/blinnphong-fs.js';
import {vertexShaderText as testShaderVStext} from './Shaders/blinnphong-vs.js';

// Entrance function
function init(){
  initializeGL();
  window.program = new Program(
    new Shader(gl.VERTEX_SHADER, testShaderVStext),
    new Shader(gl.FRAGMENT_SHADER, testShaderFStext)
  );
	window.camera = new Camera();
  console.log("Started loading models");
  let lightPos = [10,7,10];

  var monkey = {};
  //Synchronous model loading, as i am not targeting publishing nor a real game engine features. just for learning
  $.ajax({
    url: 'assets/suzanne.obj',
    dataType: 'text',
    success: function (result) {
      if (result.isOk == false)
        error.log(result.message);
      else {
        console.log("Model loaded.");
        monkey.result = result;
      }
    },
    async: false
  });

  monkey.obj = obj.loadMeshData(monkey.result);

  monkey.VertexBufferObject = gl.createBuffer(); //create a buffer object (null)
  gl.bindBuffer(gl.ARRAY_BUFFER, monkey.VertexBufferObject); //specify it's type and point at it for preceding operations
  gl.bufferData(gl.ARRAY_BUFFER, monkey.obj.vertices, gl.STATIC_DRAW); //put it's data into it
  //was gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(monkey.obj.vertices), gl.STATIC_DRAW); create a new Float32Array , but here it is a float32array already

  program.positionAttribLocationInShader = gl.getAttribLocation(program, 'vertPosition');
  gl.enableVertexAttribArray(program.positionAttribLocationInShader);

  program.texCoordAttribLocationInShader = gl.getAttribLocation(program, 'diffuseMapTexCoord_pervertex');
  gl.enableVertexAttribArray(program.texCoordAttribLocationInShader);

  program.normalsAttribLocationInShader = gl.getAttribLocation(program, 'normal_perVertex');
  gl.enableVertexAttribArray(program.normalsAttribLocationInShader);


	gl.vertexAttribPointer(
		program.positionAttribLocationInShader, // Attribute location
		3, // Number of elements per attribute
		gl.FLOAT, // Type of elements
		gl.FALSE,
		8 * Float32Array.BYTES_PER_ELEMENT, // Size of an individual vertex
		0 // Offset from the beginning of a single vertex to this attribute
	);

  gl.vertexAttribPointer(
		program.normalsAttribLocationInShader, // Attribute location
		3, // Number of elements per attribute
		gl.FLOAT, // Type of elements
		gl.FALSE,
		8 * Float32Array.BYTES_PER_ELEMENT, // Size of an individual vertex
		3 * Float32Array.BYTES_PER_ELEMENT // Offset from the beginning of a single vertex to this attribute
  );

  gl.vertexAttribPointer(
		program.texCoordAttribLocationInShader, // Attribute location
		2, // Number of elements per attribute
		gl.FLOAT, // Type of elements
		gl.FALSE,
		8 * Float32Array.BYTES_PER_ELEMENT, // Size of an individual vertex
		6 * Float32Array.BYTES_PER_ELEMENT // Offset from the beginning of a single vertex to this attribute
  );

  //
	// Create texture
	// texture (0,0) for GL is defined on the bottom left corner
  // but for the image coming from the html data, it's defined on the top left corner
  // so this caused an error that the texture seemed to be flipped vertically and that ruined the UV mapping at all
  // -- fixed it by flipping the texture images vertically :D
	var monkeyTexture = gl.createTexture();
	gl.bindTexture(gl.TEXTURE_2D, monkeyTexture);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
	gl.texImage2D(
		gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA,
		gl.UNSIGNED_BYTE,
		document.getElementById('textureImg') //data
	);
	gl.bindTexture(gl.TEXTURE_2D, null);


  //what does this do ?
  gl.useProgram(program);

  monkey.matWorldUniformLocation = gl.getUniformLocation(program, 'mWorld');
	camera.matViewUniformLocation = gl.getUniformLocation(program, 'mView');
	camera.matProjUniformLocation = gl.getUniformLocation(program, 'mProj');
  camera.posWorldUniformLocation = gl.getUniformLocation(program, 'cameraPos_world');
  let ambientFactorUniformLoc = gl.getUniformLocation(program, 'ambientFactor');
  let lightPosUniformLoc = gl.getUniformLocation(program, 'lightPos_world');

  monkey.worldMatrix = new Float32Array(16);
  mat4.identity(monkey.worldMatrix);

  //set the uniforms in the GPU side
  // (uniform Location , Transpose ? , data)
  gl.uniformMatrix4fv(monkey.matWorldUniformLocation, gl.FALSE, monkey.worldMatrix);
  gl.uniformMatrix4fv(camera.matViewUniformLocation, gl.FALSE, camera.viewMatrix);
  gl.uniformMatrix4fv(camera.matProjUniformLocation, gl.FALSE, camera.projMatrix);
  gl.uniform1f(ambientFactorUniformLoc, 0.15);
  gl.uniform3fv(lightPosUniformLoc, lightPos);

  ///////////////////////////////////////
  ////////////////////////////////////////

  var xRotationMatrix = new Float32Array(16);
  var yRotationMatrix = new Float32Array(16);

  let then=0;
  window.deltaTime = 0;
  var angle = 0;
  var identityMat = new Float32Array(16);
  mat4.identity(identityMat);
  var loop = function(){ //performance.now() : number of millis since start
    window.deltaTime = performance.now() - then;
    then = performance.now();

    //camera keyboard control only (not mouse) as handling the keys by events is laggy (because of long press key delay)
    if (camera.clickSaver.pressed){
      let e = { customKey : camera.clickSaver.key};
      camera.computeMatricesFromInputs(e);
      var viewMatrix = camera.getViewMatrix();
      gl.uniformMatrix4fv(camera.matViewUniformLocation, gl.FALSE, viewMatrix);
    }
    gl.uniform3fv( camera.posWorldUniformLocation,camera.position)

    angle = performance.now() / 1000 / 6  * Math.PI;
    mat4.rotate(yRotationMatrix, identityMat, angle, [0, 1, 0]);
		mat4.rotate(xRotationMatrix, identityMat, angle / 6, [1, 0, 0]);
    mat4.mul(monkey.worldMatrix, identityMat, yRotationMatrix);
    mat4.mul(monkey.worldMatrix, yRotationMatrix, xRotationMatrix);
    gl.uniformMatrix4fv(monkey.matWorldUniformLocation, gl.FALSE, monkey.worldMatrix);

    //msh faker dool by3mlo a 5alessss!
    //bs mngherhom hna mashtghalsh
    gl.bindTexture(gl.TEXTURE_2D, monkeyTexture);
    gl.activeTexture(gl.TEXTURE0);

    gl.clearColor(51/255, 102/255, 183/255, 1);
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
    gl.drawArrays(gl.TRIANGLES, 0, monkey.obj.vertexCount); //for normally drawing points
    //gl.drawElements(gl.TRIANGLES, Box.Indices.length, gl.UNSIGNED_SHORT, 0); //for drawing using indices concept

    requestAnimationFrame(loop);
  }
  requestAnimationFrame(loop); //di 7aga fi a ? di bt5alih hwa el yndah el function msh while true . we kman lw ra7 le tab tania bybatal y loop
}
window.addEventListener('load',init);
