
export class Shader{
  constructor (GL_TYPE, shaderText){

    this.shader    = gl.createShader(GL_TYPE);
    gl.shaderSource(this.shader, shaderText);
    gl.compileShader(this.shader);

    if (!gl.getShaderParameter(this.shader, gl.COMPILE_STATUS)){
      if (GL_TYPE == gl.VERTEX_SHADER){
        console.error("Error compiling vertex shader!", gl.getShaderInfoLog(this.shader));
        throw "Vertex shader compilation failed";
      } else {
        console.error("Error compiling fragment shader!", gl.getShaderInfoLog(this.shader));
        throw "Fragment shader compilation failed";
      }
    }

    return this.shader;

  }
}
