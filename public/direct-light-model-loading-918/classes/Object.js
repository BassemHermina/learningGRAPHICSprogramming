

//importing objLoader here caused an untraced error, m3rfsh leeh baz
//lama 3mlt import hena (gowa 7aga ma3molaha import y3ni) ..
//fa ba3at obj 3ala tool badal ma a3mlo import tani

export class Object{

  /*
  * this should load the object given the file name, and since the texture images
  * are loaded by html page, so this constructor takes the image id only
  * it should:
    - load the obj [done]
    - generate the buffer and fill it [done]
    - create the texture object (TEXTURE_2D) and get handle on texture ID [done]
    - get handle on all the uniforms/attributes required for sending to the vs/fs (custom names) [done]
    - initialize world matric (transformation matrices)
  */
  constructor(obj, filename, textureIDinHTML){

    /*
    * OBJECT PARAMETERS:
    * result              : the loading of file result (array of .txt)
    * obj                 : the objloaded array containing vertices, normals, uvs
    * VertexBufferObject  : the vertex buffer object (containing all vertices.normals.uvs not only vertices)
    * textureID           : openGL texture ID, used to refer to the texture, to bind the texture
    * worldMatrix         : the M from MVP (model matrix), transforms from model space to world space
    * transformation
    * rotation
    * scale
    */
    let me = this; //used to refer to 'this' object inside the jQuerry context
    this.result = null;
    this.obj = null;
    this.VertexBufferObject = null;
    this.textureID = null;
    this.worldMatrix = new Float32Array(16);
    this.translation = [0,0,0];
    this.rotation = quat.create();
    this.scale = [1,1,1];
    // ===============================================

    $.ajax({
      url: 'assets/' + filename,
      dataType: 'text',
      success: function (result) {
        if (result.isOk == false){
          error.log(result.message);
        } else {
          console.log(filename + " model loaded.");
          me.result = result;

          me.obj = obj.loadMeshData(me.result);
          // ===============================================

          me.VertexBufferObject = gl.createBuffer(); //create a buffer object (null)
          gl.bindBuffer(gl.ARRAY_BUFFER, me.VertexBufferObject); //specify it's type and point at it for preceding operations
          gl.bufferData(gl.ARRAY_BUFFER, me.obj.vertices, gl.STATIC_DRAW); //put it's data into it

          // ===============================================

          me.textureID = gl.createTexture(); //create a texture object and get it's id
          gl.bindTexture(gl.TEXTURE_2D, me.textureID); //bind the newly created texture object
          gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE); //set parameters
          gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
          gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
          gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
          gl.texImage2D(  //copy the data into the texture object currently bound
            gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA,
            gl.UNSIGNED_BYTE,
            document.getElementById(textureIDinHTML) //data
          );
          gl.bindTexture(gl.TEXTURE_2D, null); //bind texture null, so as to not edit the created texture by mistake

          // ===============================================

          //these are global for the program itself, loaded in app.js
          //program.positionAttribLocationInShader = gl.getAttribLocation(program, 'vertPosition');
          //program.texCoordAttribLocationInShader = gl.getAttribLocation(program, 'diffuseMapTexCoord_pervertex');
          //program.normalsAttribLocationInShader = gl.getAttribLocation(program, 'normal_perVertex');
          //program.matWorldUniformLocation = gl.getUniformLocation(program, 'mWorld');
          //mat4.identity(me.worldMatrix);
          window.scene.push(me);

        }
      }
    });

  }





  /*
  * this should be a general update/draw path for each object using the standard phong shader
  * steps are:
    - update model matrix (view and projection are updated in camera class)
    - update MVP matrix of shader (send the uniforms y3ni)
    - send data to buffers (el attributes y3ni?)
    - glDrawArrays
    - disable attributes , da mohem?
  */
  drawPhong(){
    // UPDATE MODEL MATRIX [CPU]
    mat4.fromRotationTranslationScale(this.worldMatrix, this.rotation, this.translation, this.scale);
    // =====================================
    // UPDATE MVP MATRIX [GPU] -- VP are updated by camera
    gl.uniformMatrix4fv(program.matWorldUniformLocation, gl.FALSE, this.worldMatrix);
    // =====================================
    // SEND DATA TO BUFFERS [GPU]
    gl.bindBuffer(gl.ARRAY_BUFFER, this.VertexBufferObject);
    gl.vertexAttribPointer(program.positionAttribLocationInShader, 3, gl.FLOAT, gl.FALSE, 8 * Float32Array.BYTES_PER_ELEMENT, 0);
    gl.vertexAttribPointer(program.normalsAttribLocationInShader, 3, gl.FLOAT, gl.FALSE, 8 * Float32Array.BYTES_PER_ELEMENT, 3 * Float32Array.BYTES_PER_ELEMENT);
    gl.vertexAttribPointer(program.texCoordAttribLocationInShader, 2, gl.FLOAT, gl.FALSE, 8 * Float32Array.BYTES_PER_ELEMENT, 6 * Float32Array.BYTES_PER_ELEMENT);

    gl.activeTexture(gl.TEXTURE0);
    gl.bindTexture(gl.TEXTURE_2D, this.textureID);
    // =====================================
    gl.drawArrays(gl.TRIANGLES, 0, this.obj.vertexCount);
    // =====================================
    //disableAttribsAfterDraw_Phong();

  }

  settranslation(vec){
    this.translation = vec;
  }
  setrotation(vec){
    quat.fromEuler(this.rotation, vec[0], vec[1], vec[2]);
  }
  setscale(vec){
    this.scale = vec;
  }




}
