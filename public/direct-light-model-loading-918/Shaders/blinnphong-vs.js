export var vertexShaderText = `precision highp float;

/*
* attribute (data sent from program as an array, program run as loop on them)
*===========
* vertPosition: da el vertex position, el hwa 3adad finite 3adi
* normal: da el vertex normal, el hwa nafs 3adad el vertices 3adi
* vertTexCoord: the (u,v) positions, nafs 3adad el vertices we sent mn l js brdo
*/
attribute vec3 vertPosition;
attribute vec3 normal_perVertex;
attribute vec2 diffuseMapTexCoord_pervertex;


/*
* varying (data interpolated by hardware automatically to be sent to fragmentshader per pixel)
*          we wade7 eni lma ba3ml varying we asaweeh be value m3aia per vertex, hwa by3ml interpolate fl varying da
*=========
* vertexPos_perFragment_world: da el vertex position, interpolated 3al fragments brdo
* normal_perFragment: da el vertex normal, interpolated 3ashan ykon smooth transition
* diffuseMapTexCoord_perfragment: da el (u,v) ba3d ma et3malhom interpolate 3ashan ygibo kol l no2at
*/
varying vec3 vertexPos_perFragment_world;
varying vec3 normal_perFragment;
varying vec2 diffuseMapTexCoord_perfragment;


/*
* unifroms (data sent from program as a single value, constant through the loop, so no need to interpolate, no need for varying)
*=========
* mWorld: used to transfer to world coordinates, rotate, scale and translate from the origin (0,0) relative to all objects
* mView: used to convert the space into camera space (camera is 0,0,0 looking at -Z)
* mProj: used to flatten the space AND create the perspective [ex: at orthogonal view, it multiplies z by zero so every thing is flattened]
*/
uniform mat4 mWorld;
uniform mat4 mView;
uniform mat4 mProj;


void main(){
  diffuseMapTexCoord_perfragment = diffuseMapTexCoord_pervertex;
  normal_perFragment = (mWorld * vec4(normal_perVertex, 0.0)).xyz;
  // normal_perFragment = normal_perVertex;   el satr da kan keda we badelto bl foo2 3ashan l normals telef m3aia, bs da 5ala el normals t-scale fa lw non uniform scale hy5araf
  // we le sbab msh fahmo , lma el normals el w bta3hom ykon 1.0 by5araf
  gl_Position = mProj * mView * mWorld * vec4(vertPosition, 1.0);
  vertexPos_perFragment_world = (mWorld * vec4(vertPosition, 1.0)).xyz; //light shall be calculated in a pre-projection space
                                                                  //so the pos is calculated into wolrd space only
}
`;
