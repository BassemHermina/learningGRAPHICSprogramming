export var fragmentShaderText = `precision highp float;


/*
* attribute [from CPU] (data sent from program as an array, program run as loop on them)
* ==========
* none, as they are sent to the vertex shader
*/


/*
* varying [from VS] (data interpolated by hardware automatically and sent here per pixel)
* ========
* diffuseMapTexCoord_perfragment: da el (u,v) ba3d ma et3malhom interpolate 3ashan ygibo kol l no2at
*/
varying vec3 vertexPos_perFragment_world;
varying vec3 normal_perFragment;
varying vec2 diffuseMapTexCoord_perfragment;


/*
* unifroms (data sent from program as a single value, constant through the loop, so no need to interpolate, no need for varying)
*=========
* diffuseMapSampler: contains the texture sent from the js, but how did i choose the texture and bind it to this value? y3ni lw fi aktr mn texture hy7sal a ?
*/
uniform sampler2D diffuseMapSampler;
uniform float ambientFactor;
uniform vec3 cameraPos_world;
uniform vec3 lightPos_world;

// http://www.cs.utexas.edu/~bajaj/graphics2012/cs354/lectures/lect14.pdf
// el warning el tale3 mn el shader da 3ashan fi 7agat attribute ana ba3etha we msh basta5demha

/*
  needed dummy values for calculation
  NB: needed vectors for blinn-phong
   - normal [x]
   - view [x]
   - light [x]
   - reflection [x]
   - h [ ]
*/
vec3 ambientPartColor;
vec3 diffusePartColor;
vec3 specularPartColor;
vec3 viewVector;
vec3 lightVector;
vec3 reflectionVector;
float theta;

void main()
{
  //VIEW VECTOR : v
  viewVector = cameraPos_world - vertexPos_perFragment_world;

  //LIGHT VECTOR : l
  lightVector = lightPos_world - vertexPos_perFragment_world;

  //REFLECTION VECTOR : r
  reflectionVector = 2.0 * dot(normal_perFragment,lightVector) * normal_perFragment - lightVector; //i don't yet understand this

  // SOME DUMMY TESTING ANGLES
  //theta = 1.0 - (dot(viewVector, normal_perFragment)/3.0);
  //theta = 1.0 - dot(lightVector, normal_perFragment);

  //SHADING MODEL EQUATION
  //AMBIENT PART COLOR
  ambientPartColor = ambientFactor * texture2D(diffuseMapSampler, diffuseMapTexCoord_perfragment).xyz;
  //DIFFUSE PART COLOR (the 1.0 should be replaced by a diffuse reflection coeff. map)
  diffusePartColor = 1.0 * ( max(0.0, dot( normalize(normal_perFragment), normalize(lightVector) ) ) ) * texture2D(diffuseMapSampler, diffuseMapTexCoord_perfragment).xyz;
  //DIFFUSE PART COLOR (the 1.0 should be replaced by a specular reflection coeff. map, and the power value should be replaced by shininess map)
  specularPartColor = 1.0 * pow ( max(0.0,dot( normalize(reflectionVector), normalize(viewVector) ) ), 30.0 ) * texture2D(diffuseMapSampler, diffuseMapTexCoord_perfragment).xyz;
                                  //hya leh mn gher l max di kant bt5araf, kan mktob fl ketab enena msh m7tagenha el mfrod 3ashan hya htb2a demneyan fi 7aga tania
  gl_FragColor = vec4( ambientPartColor + diffusePartColor + specularPartColor ,1.0) ;
                                  //diffuse and specular parts should be multiplied by attenuation factor in point light model
}
`;
