"use strict";

// NOTE
// this javascript logic file is designed to run in a synchronus manner
// so it is easily understood, you can first check the imported files (as they are run when imported)
// then go with the init() function untill the end

import {Program} from './classes/Program.js'
import {Shader} from './classes/Shader.js';
import {initializeGL} from './init.js';
import {Camera} from './classes/Camera.js';
import {obj} from './classes/objLoader.js';
import {Object} from './classes/Object.js';

import {fragmentShaderText as testShaderFStext} from './Shaders/blinnphong-fs.js';
import {vertexShaderText as testShaderVStext} from './Shaders/blinnphong-vs.js';

// Entrance function
function init(){
  initializeGL();
  window.program = new Program(
    new Shader(gl.VERTEX_SHADER, testShaderVStext),
    new Shader(gl.FRAGMENT_SHADER, testShaderFStext)
  );
	window.camera = new Camera();

  console.log("Started loading models");
  let lightPos = [10,7,10];

  window.scene = []; //array of to be rendered objects
  let box1 = new Object(obj, 'brb2.obj', 'textureImg2');
  let monkey1 = new Object(obj, 'suzanne.obj', 'textureImg');
  //let plane1 = new Object(obj, 'plane.obj', 'planeTex');
  //plane1.setscale([100,0.01,100]);
  //plane1.settranslation([0,-0.09,0]);
  let deer = new Object(obj, 'deer.obj', 'offwhite');
  deer.setscale([0.3,0.3,0.3]);
  let cat = new Object(obj, 'cat.obj', 'cat');
  cat.setscale([0.5,0.5,0.5]);
  cat.setrotation([0,40,0]);
  let iron = new Object(obj, 'ironman.obj', 'ironman');
  iron.settranslation([0,-5,2]);
  iron.setscale([2,2,2])
  box1.settranslation([0,5,0]);
  monkey1.settranslation([3,1,0]);
  monkey1.setrotation([0,-40,-30]);
  box1.setrotation([70,90,30]);
  let y = 0;

  // shader initializations, used in rendering function of objects
  program.positionAttribLocationInShader = gl.getAttribLocation(program, 'vertPosition');
  gl.enableVertexAttribArray(program.positionAttribLocationInShader);

  program.texCoordAttribLocationInShader = gl.getAttribLocation(program, 'diffuseMapTexCoord_pervertex');
  gl.enableVertexAttribArray(program.texCoordAttribLocationInShader);

  program.normalsAttribLocationInShader = gl.getAttribLocation(program, 'normal_perVertex');
  gl.enableVertexAttribArray(program.normalsAttribLocationInShader);

  gl.useProgram(program);

  program.matWorldUniformLocation = gl.getUniformLocation(program, 'mWorld');
	program.matViewUniformLocation = gl.getUniformLocation(program, 'mView');
	program.matProjUniformLocation = gl.getUniformLocation(program, 'mProj');
  program.camposWorldUniformLocation = gl.getUniformLocation(program, 'cameraPos_world');
  program.ambientFactorUniformLoc = gl.getUniformLocation(program, 'ambientFactor');
  program.lightPosUniformLoc = gl.getUniformLocation(program, 'lightPos_world');

  //set the uniforms in the GPU side
  // (uniform Location , Transpose ? , data)
  gl.uniformMatrix4fv(program.matViewUniformLocation, gl.FALSE, camera.viewMatrix);
  gl.uniformMatrix4fv(program.matProjUniformLocation, gl.FALSE, camera.projMatrix);
  gl.uniform1f(program.ambientFactorUniformLoc, 0.15);
  gl.uniform3fv(program.lightPosUniformLoc, lightPos);

  ///////////////////////////////////////
  ////////////////////////////////////////

  let then=0;
  window.deltaTime = 0;

  var loop = function(){
    //performance.now() : number of millis since start
    window.deltaTime = performance.now() - then;
    then = performance.now();

    //camera keyboard control only (not mouse) as handling the keys by events is laggy (because of long press key delay)
    if (camera.clickSaver.pressed){
      let e = { customKey : camera.clickSaver.key};
      camera.computeMatricesFromInputs(e);
      var viewMatrix = camera.getViewMatrix();
      gl.uniformMatrix4fv(program.matViewUniformLocation, gl.FALSE, viewMatrix);
    }
    gl.uniform3fv(program.camposWorldUniformLocation,camera.position)

    gl.clearColor(51/255, 102/255, 183/255, 1);
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    for (let i = 0; i < scene.length; i++)
      scene[i].drawPhong();
    y--;
    monkey1.setrotation([0,y,-30]);
    cat.setrotation([y,40,y]);

    requestAnimationFrame(loop);
  }
  requestAnimationFrame(loop); //di 7aga fi a ? di bt5alih hwa el yndah el function msh while true . we kman lw ra7 le tab tania bybatal y loop
}
window.addEventListener('load',init);
