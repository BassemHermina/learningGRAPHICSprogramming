export var vertexShaderText = `precision mediump float;
attribute vec3 vertPosition;
//attribute vec3 normal;
//varying vec3 normalVar;

attribute vec2 vertTexCoord;
varying vec2 fragTexCoord;

uniform mat4 mWorld;
uniform mat4 mView;
uniform mat4 mProj;

//first get position
//then multiply it by world matrix to rotate it in space
//then multiply it by view matrix to convert the space into camera space (camera is 0,0,0 looking at -Z)
//then multiply it by projection matrix to flatten the space AND create the perspective
void main(){
  fragTexCoord = vertTexCoord;
  gl_Position = mProj * mView * mWorld * vec4(vertPosition, 1.0);
  //gl_Position = vec4(vertPosition, 1.0);
}
`;
