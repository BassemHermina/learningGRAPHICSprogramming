export class Program{
  constructor(vsShader, fsShader){
    var program = gl.createProgram();
    gl.attachShader(program, vsShader);
    gl.attachShader(program, fsShader);
    gl.linkProgram(program);
    if (!gl.getProgramParameter(program, gl.LINK_STATUS)){
      console.error("Error linking program!", gl.getProgramInfoLog(program));
      return;
    }
    gl.validateProgram(program);
    if (!gl.getProgramParameter(program, gl.VALIDATE_STATUS)){
      console.error("Error validating program!", gl.getProgramInfoLog(program));
      return;
    }


    // the program should have an interface to talk to the Object class
    // and be able to tell the object that some parameters are missing 

    return program;
  }
}
